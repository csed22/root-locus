package draw;

import java.util.ArrayList;
import java.util.Arrays;

import equations.Hyperbola;
import equations.Point;
import equations.Polynomial;

public final class FunctionUtility {

	private static final int MIN_PRECISION = 30;
	private static final int MAX_PRECISION = 300;

	private FunctionUtility() {
		throw new IllegalStateException("Utility class");
	}

	public static Point[] getLine(Point start, Point end) {

		Point[] line = new Point[MAX_PRECISION + 1];

		double deltaY = end.y - start.y;
		double deltaX = end.x - start.x;
		double stepY = deltaY / MAX_PRECISION;
		double stepX = deltaX / MAX_PRECISION;

		double currentY = start.y;
		double currentX = start.x;

		line[0] = start;

		for (int i = 1; i < MAX_PRECISION; i++) {
			currentY += stepY;
			currentX += stepX;
			line[i] = new Point(currentX, currentY);
		}

		// just to make sure it is exact
		line[MAX_PRECISION] = end;

		return line;
	}

	public static Point[] getCentriod(Point[] poles) {

		Point[] centriod = new Point[1];

		double sumY = 0;
		double sumX = 0;

		for (Point p : poles) {
			sumY += p.y;
			sumX += p.x;
		}

		int n = poles.length;
		centriod[0] = new Point(sumX / n, sumY / n);

		return centriod;
	}

	public static double[] calculateAsymptoteAngles(Point[] poles) {
		int n = poles.length;
		double[] angles = new double[n];

		for (int i = 0; i < n; i++) {
			angles[i] = ((2.0 * i + 1.0) / n) * 180;
			angles[i] = Math.toRadians(angles[i]);
		}

		return angles;
	}

	public static Point[] getAsymptotes(Point[] centriod, double[] asymptoteAngles) {

		ArrayList<Point> lines = new ArrayList<>();

		for (double angle : asymptoteAngles) {
			lines.addAll(Arrays.asList(getLine(centriod[0], angle)));
		}

		return lines.toArray(new Point[1]);
	}

	private static Point[] getLine(Point centriod, double angle) {

		Point[] line = new Point[MIN_PRECISION];

		double stepY = Math.sin(angle) * 0.5 * MAX_PRECISION / MIN_PRECISION;
		double stepX = Math.cos(angle) * 0.5 * MAX_PRECISION / MIN_PRECISION;

		double currentY = centriod.y;
		double currentX = centriod.x;

		for (int i = 0; i < MIN_PRECISION; i++) {
			currentY += stepY;
			currentX += stepX;
			line[i] = new Point(currentX, currentY);
		}

		return line;

	}

	public static Point[] findBreakAways(Point[] poles) {

		Polynomial poly = new Polynomial(poles);

		ArrayList<Point> realPoles = new ArrayList<>();

		for (Point p : poles) {
			if (Math.abs(p.y) < 1e-5) {
				realPoles.add(p);
			}
		}

		Point[] breakAway = null;

		for (int i = 1; i < realPoles.size(); i++) {
			breakAway = getMinValue(poly, realPoles.get(i - 1), realPoles.get(i));
		}

		return breakAway;
	}

	private static Point[] getMinValue(Polynomial poly, Point start, Point end) {

		Point[] breakAway = new Point[1];

		double deltaX = end.x - start.x;
		double stepX = deltaX / MAX_PRECISION;

		double currentX = start.x, minX = start.x;
		double minY = poly.calcY(currentX);

		for (int i = 0; i < MAX_PRECISION + 1; i++) {
			currentX += stepX;
			double yVal = poly.calcY(currentX);
			/**
			 * WHY MIN ONLY ?? The values of s for which the dK/ds value is "POSITIVE" are
			 * the break points, recall k = -1 * poly. Min values are critical points so the
			 * derivative will be almost zero ... perfect solution to avoid complex math!!
			 */
			if (yVal < minY) {
				minX = currentX;
				minY = yVal;
			}

		}

		breakAway[0] = new Point(minX, minY);

		return breakAway;
	}

	public static Point[] getHyperbola(Point center, double asymptoteAngle, Point passing) {

		Hyperbola hyp = new Hyperbola(center, asymptoteAngle, passing);

		Point[] points = new Point[MAX_PRECISION * 2];

		int sign = 1;

		if (center.x > passing.x)
			sign = -1;

		// Low intial step ...
		double stepX = sign * 0.001 * MAX_PRECISION / MIN_PRECISION;

		double currentX = passing.x;

		for (int i = 0; i < MAX_PRECISION; i++) {
			double[] y = hyp.calcY(currentX);
			points[i] = new Point(currentX, y[0]);
			points[i + MAX_PRECISION] = new Point(currentX, y[1]);
			currentX += stepX + i * sign * 0.0002 * MAX_PRECISION / MIN_PRECISION;
		}

		return points;
	}

}
