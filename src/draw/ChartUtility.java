package draw;

import java.io.File;
import java.util.List;

import equations.Point;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import main.Main;

@SuppressWarnings({ "unchecked" })
public final class ChartUtility {

	private ChartUtility() {
		throw new IllegalStateException("Utility class");
	}

	public static Scene generateChart(String[] seriesesNames, List<Point[]> data) {

		// defining the axes
		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Imaginary Axis");
		xAxis.setLabel("Real Axis");

		// creating the charts
		final ScatterChart<Number, Number> scatterChart = new ScatterChart<>(xAxis, yAxis);
		scatterChart.setId("root-locus-diagram");
		scatterChart.setTitle("Root Locus");

		Series<Number, Number>[] serieses = defineSerieses(seriesesNames, data);

		for (Series<Number, Number> s : serieses) {
			scatterChart.getData().add(s);
		}

		Scene scene = adjustingLayout(scatterChart);

		return scene;

	}

	static Button button = new Button("Next Step");

	private static Scene adjustingLayout(ScatterChart<Number, Number> scatterChart) {

		// adjusting the layout
		// stackoverflow.com/questions/44650084/how-to-add-and-align-button-at-center-below-the-graph-with-controller-file-in-ja/44675576
		StackPane spLineChart = new StackPane();
		spLineChart.getChildren().add(scatterChart);

		// Using Buttons to Manager Chart Data
		// https://docs.oracle.com/javafx/2/charts/scatter-chart.htm
		final HBox hbox = new HBox();

		button.setOnMouseClicked((event) -> {
			if (Main.step >= 6) {
				button.setText("Close");
			}
			Main.advance();
		});

		hbox.getChildren().addAll(button);
		hbox.setPadding(new Insets(5, 0, 10, 0));

		// Positioning the button
		// https://stackoverflow.com/questions/27648860/how-to-position-a-button-in-hbox-javafx
		hbox.setAlignment(Pos.CENTER);

		StackPane spButton = new StackPane();
		spButton.getChildren().add(hbox);

		VBox vbox = new VBox();
		VBox.setVgrow(spLineChart, Priority.ALWAYS); // Make line chart always grow vertically
		vbox.getChildren().addAll(spLineChart, spButton);

		Scene scene = new Scene(vbox, 800, 600);

		// https://blog.idrsolutions.com/2014/04/use-external-css-files-javafx/
		importStyleSheet(scene);
		return scene;
	}

	private static void importStyleSheet(Scene scene) {
		File f = new File("root-locus.css");
		scene.getStylesheets().clear();
		scene.getStylesheets().add("file:///" + f.getAbsolutePath().replace("\\", "/"));
	}

	private static Series<Number, Number>[] defineSerieses(final String[] seriesesNames, final List<Point[]> data) {
		Series<Number, Number>[] serieses = new Series[seriesesNames.length];
		for (int i = 0; i < serieses.length; i++) {
			// defining a series
			serieses[i] = new Series<>();
			serieses[i].setName(seriesesNames[i]);

			for (Point p : data.get(i)) {
				// populating the series with data
				serieses[i].getData().add(new Data<Number, Number>(p.x, p.y));
			}
		}
		return serieses;
	}

}
