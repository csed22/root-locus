package main;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import draw.ChartUtility;
import draw.FunctionUtility;
import equations.Point;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

	public static int step = 1;
	private static Stage primaryStage;

	public void start(Stage primaryStage) throws Exception {
		Main.primaryStage = primaryStage;
		setIcon();
		advance();
	}

	public static void main(String[] args) {
		launch(args);
	}

	public static void advance() {
		switch (step) {
		case 1:
			Step_1();
			break;
		case 2:
			Step_2();
			break;
		case 3:
			Step_3();
			break;
		case 4:
			Step_4();
			break;
		case 5:
			Step_5();
			break;
		case 6:
			Step_6();
			break;
		default:
			System.exit(0);
		}
		primaryStage.setTitle("Step " + step);
		step++;
	}

	static String[] seriesesNames;
	static Point[] poles = { new Point(0, 0), new Point(-25, 0), new Point(-50, 10), new Point(-50, -10) };
	static List<Point[]> data = new ArrayList<>();

	private static void Step_1() {
		String[] serieses = { "Poles" };
		seriesesNames = serieses;

		data.add(poles);
		Scene scene = ChartUtility.generateChart(seriesesNames, data);

		primaryStage.setScene(scene);
		primaryStage.show();
		String title = "Step 1: Ploting poles";
		String content = "Since the equation has no zeros,\nRule 1: There will be 4 lines (loci).";
		explain(title, content);
	}

	static Point[] centriod = FunctionUtility.getCentriod(poles);

	private static void Step_2() {
		String[] serieses = { "Poles", "Centriod" };
		seriesesNames = serieses;

		data.add(centriod);

		Scene scene = ChartUtility.generateChart(seriesesNames, data);

		primaryStage.setScene(scene);
		String title = "Step 2: Finding the centriod";
		String content = "Rule 8: All lines will go to infinity along asymptotes.\nSo we need to find out the centriod point.";
		explain(title, content);
	}

	static double[] asymptoteAngles = FunctionUtility.calculateAsymptoteAngles(poles);

	private static void Step_3() {
		String[] serieses = { "Poles", "Centriod", "Asymptotes" };
		seriesesNames = serieses;

		data.add(FunctionUtility.getAsymptotes(centriod, asymptoteAngles));

		Scene scene = ChartUtility.generateChart(seriesesNames, data);

		primaryStage.setScene(scene);
		String title = "Step 3: Drawing the Asymptotes";
		String content = "The angles of the asymtotes are 45, 135, 225, 315 respectivly.";
		explain(title, content);
	}

	private static void Step_4() {
		String[] serieses = { "Loci", "Poles", "Asymptotes", "Centriod" };
		seriesesNames = serieses;

		data.add(0, FunctionUtility.getLine(new Point(0, 0), new Point(-25, 0)));

		Scene scene = ChartUtility.generateChart(seriesesNames, data);

		primaryStage.setScene(scene);
		String title = "Step 4: The portion of the real axis";
		String content = "Rule 5: The portion of the real axis to the left of an odd number of open loop poles and zeros are part of the loci.";
		explain(title, content);
	}

	static Point[] breakAway = FunctionUtility.findBreakAways(poles);

	private static void Step_5() {

		Point passing = new Point(breakAway[0].x, 0);

		Point[] real_hyp = FunctionUtility.getHyperbola(centriod[0], asymptoteAngles[0], passing);
		Point[] combinePathes = combineArrays(data.get(0), real_hyp);

		data.set(0, combinePathes);

		Scene scene = ChartUtility.generateChart(seriesesNames, data);

		primaryStage.setScene(scene);
		String title = "Step 5: Break-away Point";
		String content = "If there are not enough poles or zeros to make a pair then the extra lines go to (if pole) or come from (if zero) infinity.\nThe break-away point in calculated by solving K'(s) = 0.";
		explain(title, content);
	}

	private static void Step_6() {

		Point passing = new Point(-50, 10);

		Point[] img_hyp = FunctionUtility.getHyperbola(centriod[0], asymptoteAngles[0], passing);
		Point[] combinePathes = combineArrays(data.get(0), img_hyp);

		data.set(0, combinePathes);

		Scene scene = ChartUtility.generateChart(seriesesNames, data);

		primaryStage.setScene(scene);
		String title = "Step 6: Complex poles";
		String content = "Rule 3: when roots are complex they occur in conjugate pairs. (Symmetric about the real line)";
		explain(title, content);
	}

	private static Point[] combineArrays(Point[] arr1, Point[] arr2) {
		ArrayList<Point> biggerArr = new ArrayList<>();
		biggerArr.addAll(Arrays.asList(arr1));
		biggerArr.addAll(Arrays.asList(arr2));
		return biggerArr.toArray(new Point[0]);
	}

	static Image icon;

	private static void explain(String header, String content) {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Explanation ...");
		alert.setHeaderText(header);
		alert.setContentText(content);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(icon);
		alert.showAndWait();
	}

	@SuppressWarnings("deprecation")
	private static void setIcon() throws MalformedURLException {
		// Add a custom icon.
		// https://stackoverflow.com/questions/6098472/pass-a-local-file-in-to-url-in-java
		icon = new Image(new File("imgs/icon.png").toURL().toString());
		primaryStage.getIcons().add(icon);
	}

}
