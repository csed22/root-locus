package equations;

public class Polynomial {

	private double[] roots;

	public Polynomial(Point[] roots) {
		int n = roots.length;
		this.roots = new double[n];

		for (int i = 0; i < n; i++) {
			this.roots[i] = roots[i].x;
		}
	}

	public double calcY(double x) {
		double result = 1;
		for (double root : roots) {
			result *= (x - root);
		}
		return result;
	}

}
