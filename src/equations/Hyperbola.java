package equations;

public class Hyperbola {

	private final double h;
	private final double a;

	private final double k;
	private final double b;

	public Hyperbola(Point center, double asymptoteAngle, Point passing) {

		h = center.x;
		k = center.y;

		a = Math.sqrt(calculateASqr(asymptoteAngle, passing));
		b = a * Math.tan(asymptoteAngle);

	}

	private double calculateASqr(double asymptoteAngle, Point passing) {

		double aVal;

		aVal = (passing.x - h);
		aVal *= aVal;
		

		double term = (passing.y - k) / Math.tan(asymptoteAngle);
		term *= term;
		aVal -= term;
		
		return aVal;
	}

	public double[] calcY(double x) {

		double[] yVals = new double[2];

		double term = (x - h) / a;
		term *= term;
		term -= 1;
		term *= (b * b);

		term = Math.sqrt(term);

		yVals[0] = k - term;
		yVals[1] = k + term;

		return yVals;

	}

//	public static void main(String[] args) {
//
//		// (-31.25, 0.0), 0.7853981633974483, (-8.999999999999995, -242064.00000000003)
//
//		Hyperbola hyp = new Hyperbola(new Point(-31.25, 0.0), 0.7853981633974483,
//				new Point(-8.999999999999995, 0));
//
//		System.out.println(hyp.a);
//		System.out.println(hyp.b);
//
//	}

}
